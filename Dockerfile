#FROM ubuntu
FROM ubuntu:20.04

# this is to allow picard write stdout
#RUN ln -s /proc/self/fd/1  /dev/stdout
RUN ln -sf /proc/self/fd/1  /dev/stdout

# noninteractive packages installation  
ENV DEBIAN_FRONTEND=noninteractive

# update 
RUN apt-get update -y 

# install ftp client
RUN apt-get install -y ftp

# install bc
RUN apt-get install -y bc

# install GNU AWK
RUN apt-get install -y gawk

# install numpy, pysam
RUN apt-get install -y python-numpy
RUN apt-get install -y python-pysam

# install wget
RUN apt-get install -y wget

# install unzip
RUN apt-get install -y unzip

# install bzip2
RUN apt-get install -y bzip2

# install make
RUN apt-get install -y make

# install make
RUN apt-get install -y gcc

# install sudo
RUN apt install sudo

# install java
#RUN apt-get install default-jre
RUN apt-get install default-jre --yes

# get picard
RUN wget https://github.com/broadinstitute/picard/releases/download/2.12.1/picard.jar

# install samtools
WORKDIR /
RUN apt-get install -y libncurses5-dev 
RUN apt-get install -y libz-dev 
RUN apt-get install -y libbz2-dev
RUN apt-get install -y liblzma-dev
RUN apt-get install -y libcurl4-gnutls-dev
RUN wget https://github.com/samtools/samtools/releases/download/1.4.1/samtools-1.4.1.tar.bz2
RUN tar -jxf samtools-1.4.1.tar.bz2 
WORKDIR samtools-1.4.1
RUN ./configure --enable-plugins --enable-libcurl --with-plugin-path=./htslib-1.4.1
RUN  make all plugins-htslib
RUN  mv ./samtools /usr/bin

# install git
RUN apt-get install -y git

# install File::Slurp
RUN apt-get install -y libfile-slurp-perl

# download TraFiC
WORKDIR /
RUN mkdir trafic
RUN git clone -b MEIBA https://gitlab.com/mobilegenomesgroup/TraFiC.git /trafic

# install bwa, tabix, bedtools, velvet
RUN apt-get install -y bwa
RUN apt-get install -y tabix
RUN apt-get install -y bedtools
RUN apt-get install -y velvet

# download blat binary 
RUN wget https://www.dropbox.com/s/npbpzpuxp9z0xwf/blat -P /usr/bin/
RUN /bin/bash -c 'chmod -R 777 /usr/bin/blat'

## install snakemake
RUN apt-get install -y snakemake

## create folders for trafic input and outputs
WORKDIR /
RUN mkdir /root/trafic_input
RUN mkdir /root/trafic_output

# download reference genome
RUN mkdir -p /reference_genome/human/hg19
RUN wget https://www.dropbox.com/s/4fdbnuc6ka6lwkx/hs37d5.tar.gz
RUN tar -xvzf /hs37d5.tar.gz  -C /reference_genome/human/hg19/
RUN mv /reference_genome/human/hg19/hs37d5.fa /reference_genome/human/hg19/genome.fa
RUN mv /reference_genome/human/hg19/hs37d5.fa.fai /reference_genome/human/hg19/genome.fa.fai
RUN /bin/bash -c 'chmod -R 777 /reference_genome'

#grant permissions to the relevant folders
RUN chmod -R 770 /root/trafic_input
RUN chmod -R 770 /root/trafic_output
RUN chmod -R 777 /trafic

# add user 'ubuntu' to run the code in the container
RUN useradd -ms /bin/bash ubuntu

# apply user 'ubuntu' to run the code in the container
USER ubuntu
WORKDIR /home/ubuntu

# default
CMD /bin/bash
